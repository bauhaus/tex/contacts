module.exports = {
  internal: {
    contacts: {
      host: process.env.LISTEN_ADDRESS || "0.0.0.0",
    },
  },
};
