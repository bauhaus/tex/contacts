FROM node:14-alpine as base

WORKDIR /app

FROM base as app

RUN apk add --no-cache git python3 make
RUN git clone https://gitlab.com/bauhaus/tex/mirror/contacts.git /app/

RUN npm ci --quiet

FROM base

RUN apk add --no-cache tini
ENTRYPOINT ["/sbin/tini", "--"]

COPY --from=app /app /app
COPY ./settings.js /app/config/settings.js

ENV SHARELATEX_CONFIG=/app/config/settings.js

CMD ["node", "--expose-gc", "app.js"]
